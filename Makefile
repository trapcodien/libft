# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/09/23 12:05:07 by garm              #+#    #+#              #
##   Updated: 2015/05/02 20:27:15 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = libft.a
LIBS = -lft

SOURCES_DIR = srcs
INCLUDES_DIR = includes

CHECK = @cppcheck -I $(INCLUDES_DIR) --enable=all --check-config --suppress=missingIncludeSystem .

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra -pedantic
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -pedantic -std=c89
endif

CFLAGS = $(FLAGS) -I $(INCLUDES_DIR)

DEPENDENCIES = \
			   $(INCLUDES_DIR)/libft.h \
			   $(INCLUDES_DIR)/libft_config.h \

SOURCES = \
			 $(SOURCES_DIR)/ft_memset.c \
			 $(SOURCES_DIR)/ft_bzero.c \
			 $(SOURCES_DIR)/ft_memalloc.c \
			 $(SOURCES_DIR)/ft_memdel.c \
			 $(SOURCES_DIR)/ft_memcpy.c \
			 $(SOURCES_DIR)/ft_memccpy.c \
			 $(SOURCES_DIR)/ft_memmove.c \
			 $(SOURCES_DIR)/ft_memchr.c \
			 $(SOURCES_DIR)/ft_memcmp.c \
			 $(SOURCES_DIR)/ft_memdup.c \
			 $(SOURCES_DIR)/ft_strnew.c \
			 $(SOURCES_DIR)/ft_strdel.c \
			 $(SOURCES_DIR)/ft_strclr.c \
			 $(SOURCES_DIR)/ft_strlen.c \
			 $(SOURCES_DIR)/ft_strlenc.c \
			 $(SOURCES_DIR)/ft_strdup.c \
			 $(SOURCES_DIR)/ft_strcpy.c \
			 $(SOURCES_DIR)/ft_strncpy.c \
			 $(SOURCES_DIR)/ft_strcat.c \
			 $(SOURCES_DIR)/ft_strncat.c \
			 $(SOURCES_DIR)/ft_strlcat.c \
			 $(SOURCES_DIR)/ft_strchr.c \
			 $(SOURCES_DIR)/ft_strrchr.c \
			 $(SOURCES_DIR)/ft_strstr.c \
			 $(SOURCES_DIR)/ft_strnstr.c \
			 $(SOURCES_DIR)/ft_strcmp.c \
			 $(SOURCES_DIR)/ft_strncmp.c \
			 $(SOURCES_DIR)/ft_strequ.c \
			 $(SOURCES_DIR)/ft_strnequ.c \
			 $(SOURCES_DIR)/ft_strsub.c \
			 $(SOURCES_DIR)/ft_strjoin.c \
			 $(SOURCES_DIR)/ft_strtrim.c \
			 $(SOURCES_DIR)/ft_strtrimc.c \
			 $(SOURCES_DIR)/ft_findc.c \
			 $(SOURCES_DIR)/ft_strcountc.c \
			 $(SOURCES_DIR)/ft_strdelmultic.c \
			 $(SOURCES_DIR)/ft_strisnum.c \
			 $(SOURCES_DIR)/ft_strsplit.c \
			 $(SOURCES_DIR)/ft_strsplit_strict.c \
			 $(SOURCES_DIR)/ft_splitdel.c \
			 $(SOURCES_DIR)/ft_splitlen.c \
			 $(SOURCES_DIR)/ft_tabmerge.c \
			 $(SOURCES_DIR)/ft_atoi.c \
			 $(SOURCES_DIR)/ft_itoa.c \
			 $(SOURCES_DIR)/ft_isascii.c \
			 $(SOURCES_DIR)/ft_isprint.c \
			 $(SOURCES_DIR)/ft_isupper.c \
			 $(SOURCES_DIR)/ft_islower.c \
			 $(SOURCES_DIR)/ft_isalpha.c \
			 $(SOURCES_DIR)/ft_isdigit.c \
			 $(SOURCES_DIR)/ft_isalnum.c \
			 $(SOURCES_DIR)/ft_iswhite.c \
			 $(SOURCES_DIR)/ft_toupper.c \
			 $(SOURCES_DIR)/ft_tolower.c \
			 $(SOURCES_DIR)/ft_ban_neg.c \
			 $(SOURCES_DIR)/ft_putchar.c \
			 $(SOURCES_DIR)/ft_putstr.c \
			 $(SOURCES_DIR)/ft_putendl.c \
			 $(SOURCES_DIR)/ft_putnbr.c \
			 $(SOURCES_DIR)/ft_putnstr.c \
			 $(SOURCES_DIR)/ft_printf.c \
			 $(SOURCES_DIR)/ft_putchar_fd.c \
			 $(SOURCES_DIR)/ft_putstr_fd.c \
			 $(SOURCES_DIR)/ft_putendl_fd.c \
			 $(SOURCES_DIR)/ft_putnbr_fd.c \
			 $(SOURCES_DIR)/ft_putnstr_fd.c \
			 $(SOURCES_DIR)/ft_fprintf.c \
			 $(SOURCES_DIR)/ft_asprintf.c \
			 $(SOURCES_DIR)/ft_error.c \
			 $(SOURCES_DIR)/ft_fatal.c \
			 $(SOURCES_DIR)/ft_striter.c \
			 $(SOURCES_DIR)/ft_striteri.c \
			 $(SOURCES_DIR)/ft_strmap.c \
			 $(SOURCES_DIR)/ft_strmapi.c \
			 $(SOURCES_DIR)/ft_lstnew.c \
			 $(SOURCES_DIR)/ft_lstdelone.c \
			 $(SOURCES_DIR)/ft_lstdel.c \
			 $(SOURCES_DIR)/ft_lstadd.c \
			 $(SOURCES_DIR)/ft_lstiter.c \
			 $(SOURCES_DIR)/ft_lstmap.c \
			 $(SOURCES_DIR)/get_next_line.c \
			 $(SOURCES_DIR)/ft_sscanf.c \
			 $(SOURCES_DIR)/ft_getopt.c \
			 $(SOURCES_DIR)/ft_getopt_control.c \
			 $(SOURCES_DIR)/ft_list.c \
			 $(SOURCES_DIR)/ft_swap.c \


OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS)
	@echo Creating $(NAME)...
	@ar rcs $(NAME) $(OBJS)

clean:
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

cleanbin:
	@rm -f $(NAME)
	@echo Deleting $(NAME)..
	
check:
	$(CHECK)

fclean: clean cleanbin
	@echo Full clean OK.

re: fclean all

.PHONY: clean fclean re all

