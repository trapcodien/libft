/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 15:56:29 by garm              #+#    #+#             */
/*   Updated: 2015/11/16 15:56:31 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_lst		*ft_lstmap(t_lst *lst, t_lst *(*f)(t_lst *elem))
{
	t_lst	*new;
	t_lst	*next_new;

	new = NULL;
	if (!f || !lst)
		return (new);
	new = f(lst);
	next_new = ft_lstmap(lst->next, f);
	new->next = next_new;
	return (new);
}
