/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 15:52:27 by garm              #+#    #+#             */
/*   Updated: 2015/11/16 15:52:28 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lstdel(t_lst **alst, void (*del)(void *, size_t))
{
	t_lst	*cursor;
	t_lst	*prev_cursor;

	if (!alst)
		return ;
	cursor = *alst;
	while (cursor)
	{
		del(cursor->content, cursor->content_size);
		prev_cursor = cursor;
		cursor = cursor->next;
		ft_memdel((void **)&prev_cursor);
	}
	*alst = NULL;
}
