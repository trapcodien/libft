/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 18:47:51 by garm              #+#    #+#             */
/*   Updated: 2014/06/15 20:43:16 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	size_t		i;
	char		*str1;
	const char	*str2;

	if (!s1 || !s2)
		return (s1);
	str1 = (char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n && str2[i] != (char)c)
	{
		str1[i] = str2[i];
		i++;
	}
	if (str2[i] == (char)c && i < n)
	{
		str1[i] = (char)c;
		i++;
		return ((void *)(str1 + i));
	}
	if (i >= n)
		return (NULL);
	return ((void *)(str1 + i));
}
