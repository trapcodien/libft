/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 18:39:32 by garm              #+#    #+#             */
/*   Updated: 2014/09/23 12:06:40 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memcpy(void *s1, const void *s2, size_t n)
{
	size_t			i;
	char			*str1;
	const char		*str2;

	if (!s1 || !s2)
		return (s1);
	str1 = (char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n)
	{
		str1[i] = str2[i];
		i++;
	}
	return (s1);
}
