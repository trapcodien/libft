/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 20:46:15 by garm              #+#    #+#             */
/*   Updated: 2014/10/26 01:48:09 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int nb)
{
	char			*ptr;
	static char		str[12];
	int				isneg;

	isneg = 0;
	ptr = &str[11];
	str[11] = '\0';
	if (nb == 0)
		*--ptr = '0';
	while (nb != 0)
	{
		if (nb >= 0)
			*--ptr = '0' + (nb % 10);
		else
		{
			*--ptr = '0' - (nb % 10);
			isneg = 1;
		}
		nb = nb / 10;
	}
	if (isneg)
		*--ptr = '-';
	return (ptr);
}
