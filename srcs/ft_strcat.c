/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 20:40:59 by garm              #+#    #+#             */
/*   Updated: 2014/06/21 17:10:50 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strcat(char *s1, const char *s2)
{
	char		*start_copy;

	if (!s1 || !s2)
		return (s1);
	start_copy = s1 + ft_strlen(s1);
	ft_strcpy(start_copy, s2);
	return (s1);
}
