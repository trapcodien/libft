/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 00:54:10 by garm              #+#    #+#             */
/*   Updated: 2014/06/21 23:04:10 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_swap(void *a, void *b)
{
	*(unsigned long *)a = *(unsigned long *)a ^ *(unsigned long *)b;
	*(unsigned long *)b = *(unsigned long *)a ^ *(unsigned long *)b;
	*(unsigned long *)a = *(unsigned long *)a ^ *(unsigned long *)b;
}
