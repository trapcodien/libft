/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 21:37:11 by garm              #+#    #+#             */
/*   Updated: 2014/06/25 22:48:21 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		*clean_string(const char *s, char c)
{
	char	*ret;
	char	*tmp;

	tmp = ft_strtrimc(s, c);
	ret = ft_strdelmultic(tmp, c);
	ft_strdel(&tmp);
	return (ret);
}

char			**ft_strsplit(const char *s, char c)
{
	char	*cleaned_string;
	char	**split;

	if (!s || !c)
		return (NULL);
	cleaned_string = clean_string(s, c);
	split = ft_strsplit_strict(cleaned_string, c);
	ft_strdel(&cleaned_string);
	return (split);
}
