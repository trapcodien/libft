/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 15:56:35 by garm              #+#    #+#             */
/*   Updated: 2015/11/16 15:56:36 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_lst		*ft_lstnew(void const *content, size_t content_size)
{
	t_lst	*elem;

	if (!(elem = (t_lst *)ft_memalloc(sizeof(t_lst))))
		return (NULL);
	elem->next = NULL;
	elem->content = (void *)content;
	elem->content_size = (content) ? content_size : 0;
	return (elem);
}
