/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 15:57:26 by garm              #+#    #+#             */
/*   Updated: 2015/11/16 15:57:27 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_strmap(const char *s, char (*f)(char))
{
	char			*fresh_str;
	unsigned int	i;

	if (!s || !f)
		return (NULL);
	fresh_str = ft_strnew(ft_strlen(s));
	i = 0;
	while (s[i])
	{
		fresh_str[i] = f(s[i]);
		i++;
	}
	return (fresh_str);
}
