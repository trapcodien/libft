/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 03:30:56 by garm              #+#    #+#             */
/*   Updated: 2014/06/16 03:43:14 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t		i;
	size_t		ret;

	if (!s1 || !s2)
		return (-1);
	i = 0;
	while (i < n && s1[i] && s2[i])
	{
		if ((ret = s1[i] - s2[i]) != 0)
			return (ret);
		i++;
	}
	if (i < n && (!s1[i] || !s2[i]))
		return (s1[i] - s2[i]);
	return (0);
}
