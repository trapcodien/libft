/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt_control.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/24 18:11:39 by garm              #+#    #+#             */
/*   Updated: 2014/10/24 18:13:15 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_getopt_control(int action, int *ac_pos, int *av_pos)
{
	if (action == RESET)
	{
		*ac_pos = 1;
		*av_pos = 0;
		return (*ac_pos);
	}
	else if (action == GET)
		return (*ac_pos);
	return (0);
}
