/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_asprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 21:13:03 by garm              #+#    #+#             */
/*   Updated: 2015/10/10 14:04:56 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "libft.h"
#include "libftdata.h"

static int		dbuf_writestr(t_dbuf *d, char *str)
{
	int			len;

	len = ft_strlen(str);
	return (ft_dbuf_write(d, str, len));
}

static int		dbuf_writechar(t_dbuf *d, int c)
{
	ft_dbuf_write(d, &c, 1);
	return (1);
}

static int		ft_set_ret(char **ret, t_dbuf *d, int len)
{
	int			zero;

	zero = 0;
	ft_dbuf_write(d, &zero, 1);
	*ret = d->data;
	ft_memdel((void **)&d);
	return (len);
}

static void		init_asprintf(t_dbuf **d, char *format, int *len)
{
	*d = ft_dbuf_create(ft_strlen(format));
	*len = 0;
}

int				ft_asprintf(char **ret, char *format, ...)
{
	int			offset;
	int			len;
	va_list		ap;
	t_dbuf		*dbuf;

	va_start(ap, format);
	init_asprintf(&dbuf, format, &len);
	while ((offset = ft_findc(format, '%')) != -1 && ((len += offset) > -1))
	{
		format += ft_dbuf_write(dbuf, format, offset);
		if (format[0] == '%' && format[1] == '%')
			len += ft_dbuf_write(dbuf, "%", 1);
		else if (format[0] == '%' && format[1] == 's')
			len += dbuf_writestr(dbuf, va_arg(ap, char *));
		else if (format[0] == '%' && (format[1] == 'i' || format[1] == 'd'))
			len += dbuf_writestr(dbuf, ft_itoa(va_arg(ap, int)));
		else if (format[0] == '%' && format[1] == 'c')
			len += dbuf_writechar(dbuf, va_arg(ap, int));
		else
			return (ft_set_ret(ret, dbuf, len));
		format += 2;
	}
	len += dbuf_writestr(dbuf, format);
	va_end(ap);
	return (ft_set_ret(ret, dbuf, len));
}
