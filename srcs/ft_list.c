/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/02 20:27:32 by garm              #+#    #+#             */
/*   Updated: 2015/05/02 20:50:46 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		list_add(void *head_addr, void *elem)
{
	t_list	**head;
	t_list	*e;

	head = head_addr;
	e = elem;
	e->next = NULL;
	e->prev = NULL;
	if (*head)
	{
		e->next = *head;
		if ((*head)->prev)
			e->prev = (*head)->prev;
		(*head)->prev = e;
	}
	*head = e;
}

void		list_del(void *head_addr, void *elem_addr, void (*f)(void **))
{
	t_list	**head;
	t_list	*e;

	head = head_addr;
	e = elem_addr;
	if (e == *head)
		*head = e->next;
	if (e->next)
		e->next->prev = e->prev;
	if (e->prev)
		e->prev->next = e->next;
	e->next = NULL;
	e->prev = NULL;
	f((void **)&e);
}
