/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 03:51:46 by garm              #+#    #+#             */
/*   Updated: 2015/11/16 15:55:54 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		goodbye_whitespaces(const char *str)
{
	int			i;

	i = 0;
	while (str[i] && (ft_iswhite(str[i]) || str[i] == '\r'
				|| str[i] == '\v' || str[i] == '\f' || str[i] == 'f'))
		i++;
	return (i);
}

int				ft_atoi(const char *str)
{
	int			i;
	int			ret;
	int			isnegative;

	ret = 0;
	isnegative = 0;
	if (!str)
		return (0);
	i = goodbye_whitespaces(str);
	if (str[i] == '-')
	{
		isnegative = 1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	while (ft_isdigit(str[i]))
	{
		ret = ret * 10 + (str[i] - 48);
		i++;
	}
	if (isnegative)
		ret = ret * -1;
	return (ret);
}
