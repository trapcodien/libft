/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 15:57:29 by garm              #+#    #+#             */
/*   Updated: 2015/11/16 15:57:30 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_strmapi(const char *s, char (*f)(unsigned int, char))
{
	unsigned int	i;
	char			*fresh_str;

	if (!s || !f)
		return (NULL);
	fresh_str = ft_strnew(ft_strlen(s));
	i = 0;
	while (s[i])
	{
		fresh_str[i] = f(i, s[i]);
		i++;
	}
	return (fresh_str);
}
