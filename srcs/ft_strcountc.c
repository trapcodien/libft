/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcountc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 01:21:54 by garm              #+#    #+#             */
/*   Updated: 2014/06/18 21:36:00 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcountc(const char *s, char c)
{
	int		i;
	int		counter;

	i = 0;
	counter = 0;
	while (s && s[i])
	{
		if (s[i] == c)
			counter++;
		i++;
	}
	return (counter);
}
