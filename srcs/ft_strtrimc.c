/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrimc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 19:22:53 by garm              #+#    #+#             */
/*   Updated: 2014/06/18 19:25:31 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrimc(const char *s, char c)
{
	int			start;
	int			len;
	int			cutted_end;

	len = ft_strlen(s);
	start = 0;
	while (s && s[start] && s[start] == c)
		start++;
	cutted_end = len - 1;
	while (cutted_end >= 0 && s && s[cutted_end] == c)
		cutted_end--;
	if ((len = cutted_end - start + 1) < 0)
		len = 0;
	return (ft_strsub(s, start, len));
}
