/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 03:12:52 by garm              #+#    #+#             */
/*   Updated: 2014/09/23 12:14:45 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t		i;
	size_t		len;
	size_t		len_s2;

	if (!s2 || !*s2)
		return ((char *)s1);
	i = 0;
	len = ft_strlen(s1);
	len_s2 = ft_strlen(s2);
	while (s1 && i < len && i < n)
	{
		if (n - i < len_s2)
			return (NULL);
		if (ft_memcmp(&s1[i], s2, len_s2) == 0)
			return ((char *)&s1[i]);
		i++;
	}
	return (NULL);
}
