/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_config.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 00:47:42 by garm              #+#    #+#             */
/*   Updated: 2015/05/02 20:15:38 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_CONFIG_H
# define LIBFT_CONFIG_H

# define ERROR -1
# define BUFF_SIZE 4096

/*
** BOOLEAN
*/
# define TRUE 1
# define FALSE 0

typedef int			t_bool;

/*
** ft_getopt() MACROS
*/
# define OPT_STOP_PARSE -1
# define OPT_END 0
# define OPT_UNKNOW '?'
# define RESET -2
# define GET -1
# define THE NULL
# define ARGC NULL
# define ARGV NULL
# define POSITION NULL

/*
** ft_getopt() structure
*/
typedef struct		s_optarg
{
	const char		*arg;
	int				opt;
}					t_opt;

/*
** Containers structures
*/
typedef struct		s_list
{
	struct s_list	*next;
	struct s_list	*prev;
}					t_list;

typedef struct		s_lst
{
	void			*content;
	size_t			content_size;
	struct s_lst	*next;
}					t_lst;

#endif
